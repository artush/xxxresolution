package xxx;

import aima.core.logic.propositional.algorithms.KnowledgeBase;
import aima.core.logic.propositional.algorithms.XXXResolution;

/**
 * @author Ravi Mohan
 * 
 */
public class PLResolutionDemo {
	private static XXXResolution xxxResolution = new XXXResolution();

	public static void main(String[] args) {
		KnowledgeBase kb = new KnowledgeBase();
		String fact = "(((OR P Q R) AND (Q => R)) AND (R => P))";
//        String fact = "((((OR P Q (NOT R) (NOT T)) AND (OR Q R)) AND (OR (NOT Q) (NOT T))) AND (P => T))";

        kb.tell(fact);
		System.out.println("\nPLResolutionDemo\n");
		System.out.println("adding " + fact + "to knowldegebase");
		displayResolutionResults(kb, "P");
//        displayResolutionResults(kb, "(NOT P)");
	}

	private static void displayResolutionResults(KnowledgeBase kb, String query) {
		System.out.println("Running xxxResolution of query " + query
                + " on knowledgeBase  gives " + xxxResolution.xxxResolution(kb, query));
	}
}
