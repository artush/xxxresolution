package aima.core.logic.propositional.algorithms;

import aima.core.logic.propositional.parsing.PEParser;
import aima.core.logic.propositional.parsing.ast.*;
import aima.core.logic.propositional.visitors.CNFClauseGatherer;
import aima.core.logic.propositional.visitors.CNFTransformer;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Artush
 * Date: 3/18/14
 * Time: 1:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class XXXResolution {

    public boolean xxxResolution(KnowledgeBase kb, String alpha) {
        return xxxResolution(kb, (Sentence) new PEParser().parse(alpha));
    }

    private boolean xxxResolution(KnowledgeBase kb, Sentence alpha) {
        Sentence kBAndNotAlpha = new BinarySentence("AND", kb.asSentence(),
                new UnarySentence(alpha));
        Set<Sentence> clauses = new CNFClauseGatherer()
                .getClausesFrom(new CNFTransformer().transform(kBAndNotAlpha));

        List<Set<Sentence>> independGroups = divideIntoIndependGroup(clauses);

        for (Set<Sentence> independGroup : independGroups) {
            Symbol positiveObservedSentence = getRandomSentence(independGroup);
            UnarySentence negativeObservedSentence = new UnarySentence(positiveObservedSentence);
            Set<Sentence> atomicSentences = fetchAtomicSentences(independGroup);

            if(derived(positiveObservedSentence, independGroup,atomicSentences) &&
                    derived(negativeObservedSentence, independGroup, atomicSentences)) {
                return true;
            }
        }

        return false;
    }

    private boolean derived(Sentence observedSentence, Set<Sentence> independGroup, Set<Sentence> atomicSentences) {
        //System.out.println("*****     Starting derive: " + observedSentence.toString() + "     *****");
        //System.out.println("***********************************************");
        if(independGroup.contains(observedSentence)) {
            //System.out.println(observedSentence + " contains into the independ group: " + independGroup.toString());
            return true;
        }


        Set<Sentence> waySentences = new HashSet<Sentence>();
        waySentences.add(observedSentence);

        Set<Sentence> sentencesContainedObservedSentence =
                fetchSentencesContainedObservedSentence(observedSentence, independGroup);

        /*System.out.println(observedSentence + " contains into the following sentences:");
        for (Sentence sentence : sentencesContainedObservedSentence) {
            System.out.println("-----" + sentence.toString());
        }
        System.out.println();
        System.out.println();*/

        for (Sentence sentence : sentencesContainedObservedSentence) {
            Set<Sentence> sentencesDependForDerivedObservedSentence =
                    fetchSentencesDependForDerivedObservedSentence(observedSentence, sentence);

            //System.out.println("Lets consider the " + sentence.toString() + " sentence.");
            //System.out.println("We must derive the following sentences:");
            //for (Sentence sentence1 : sentencesDependForDerivedObservedSentence) {
            //    System.out.println("....." + sentence1.toString());
            //}
            //System.out.println();

            boolean isDerived = true;
            for (Sentence symbolOrUnarySentence : sentencesDependForDerivedObservedSentence) {
                if(!derivedItem(symbolOrUnarySentence, independGroup, atomicSentences, new HashSet<Sentence>(waySentences))) {
                    isDerived = false;
                    break;
                }
            }

            if(isDerived) {
                //System.out.println("Derived: " + observedSentence.toString());
                return true;
            }
        }

        //System.out.println("NOT Derived: " + observedSentence.toString());
        return false;
    }

    private boolean derivedItem(Sentence observedSentenceItem, Set<Sentence> independGroup, Set<Sentence> atomicSentences, Set<Sentence> waySentences) {
        //System.out.println("Starting derive: " + observedSentenceItem.toString());
        //System.out.println("---------------------------------------------------");
        if(atomicSentences.contains(observedSentenceItem) || waySentences.contains(negation(observedSentenceItem))) {
            //System.out.println("Derived: " + observedSentenceItem + ", Contains into the atomic sentences or his negation contains into the way sentences.");
            return true;
        }

        if(waySentences.contains(observedSentenceItem)) {
            //System.out.println("Not derived: " + observedSentenceItem + ", Contains into the way sentences.");
            return false;
        }

        //add current observed sentence in way's sentences.
        waySentences.add(observedSentenceItem);

        Set<Sentence> sentencesContainedObservedSentence =
                fetchSentencesContainedObservedSentence(observedSentenceItem, independGroup);

        //System.out.println(observedSentenceItem + " contains into the following sentences:");
        //for (Sentence sentence : sentencesContainedObservedSentence) {
        //    System.out.println("-----" + sentence.toString());
        //}
        //System.out.println();
        //System.out.println();

        for (Sentence sentence : sentencesContainedObservedSentence) {
            Set<Sentence> sentencesDependForDerivedObservedSentence =
                    fetchSentencesDependForDerivedObservedSentence(observedSentenceItem, sentence);

            //System.out.println("Lets consider the " + sentence.toString() + " sentence.");
            //System.out.println("We must derive the following sentences:");
            //for (Sentence sentence1 : sentencesDependForDerivedObservedSentence) {
            //    System.out.println("....." + sentence1.toString());
            //}
            //System.out.println();

            boolean isDerived = true;
            for (Sentence symbolOrUnarySentence : sentencesDependForDerivedObservedSentence) {
                if(!derivedItem(symbolOrUnarySentence, independGroup, atomicSentences, new HashSet<Sentence>(waySentences))) {
                    isDerived = false;
                    break;
                }
            }

            if(isDerived) {
                //System.out.println("Derived: " + observedSentenceItem.toString());
                return true;
            }
        }

        //System.out.println("NOT Derived: " + observedSentenceItem.toString());
        return false;
    }

    private Set<Sentence> fetchAtomicSentences(Set<Sentence> independGroup) {
        Set<Sentence> atomicSentences = new HashSet<Sentence>();
        for (Sentence groupSentence : independGroup) {
            if(groupSentence instanceof Symbol || groupSentence instanceof UnarySentence) {
                atomicSentences.add(groupSentence);
            }
        }
        return atomicSentences;
    }

    /**
     * Fetch sentences from clause which must derived for derive observed sentence.
     * @param observedSentence      Observed sentence.
     * @param sentence              Clause which contains observed sentence.
     * @return                      Return Symbols or UnarySentence contained into the sentence, except observed sentence.
     */
    private Set<Sentence> fetchSentencesDependForDerivedObservedSentence(Sentence observedSentence, Sentence sentence) {
        Set<Sentence> sentencesDependForDerivedObservedSentence = new HashSet<Sentence>();
        if(sentence instanceof BinarySentence) {
            if(observedSentence.equals( ((BinarySentence)sentence).getFirst() )) {
                sentencesDependForDerivedObservedSentence.add(negation(((BinarySentence)sentence).getSecond()));
            } else {
                sentencesDependForDerivedObservedSentence.add(negation(((BinarySentence)sentence).getFirst()));
            }
        } else {
            List<Sentence> sentenceList = ((MultiSentence)sentence).getSentences();
            for (Sentence sentenceListItem : sentenceList) {
                if(!observedSentence.equals(sentenceListItem)) {
                    sentencesDependForDerivedObservedSentence.add(negation(sentenceListItem));
                }
            }
        }

        return sentencesDependForDerivedObservedSentence;
    }

    /**
     * Return negation for provided sentence.
     * @param sentence   Provided sentence.
     * @return           Negation given sentence.
     */
    private Sentence negation(Sentence sentence) {
        if(sentence instanceof Symbol) {
            return new UnarySentence(sentence);
        } else {
            return ((UnarySentence)sentence).getNegated();
        }
    }

    private Set<Sentence> fetchSentencesContainedObservedSentence(Sentence observedSentence, Set<Sentence> sentences) {
        Set<Sentence> sentencesContainedObservedSentence = new HashSet<Sentence>();
        for (Sentence sentence : sentences) {
            if(sentence instanceof Symbol || sentence instanceof UnarySentence) {
                if(sentence.equals(observedSentence)) {
                    sentencesContainedObservedSentence.add(sentence);
                }
            } else if(sentence instanceof BinarySentence) {
                if( ((BinarySentence)sentence).getFirst().equals(observedSentence) ||
                        ((BinarySentence)sentence).getSecond().equals(observedSentence)) {
                    sentencesContainedObservedSentence.add(sentence);
                }
            } else {
                if(((MultiSentence)sentence).getSentences().contains(observedSentence)) {
                    sentencesContainedObservedSentence.add(sentence);
                }
            }
        }
        return sentencesContainedObservedSentence;
    }

    private Symbol getRandomSentence(Set<Sentence> sentences) {
        Sentence randomSentence = sentences.iterator().next();
        return getRandomSymbol(randomSentence);
    }

    private Symbol getRandomSymbol(Sentence sentence) {
        if(sentence instanceof Symbol) {
            return (Symbol) sentence;
        } else if (sentence instanceof UnarySentence) {
            return getRandomSymbol(((UnarySentence) sentence).getNegated());
        } else if (sentence instanceof BinarySentence) {
            return getRandomSymbol(((BinarySentence) sentence).getFirst());
        } else {
            return getRandomSymbol(((MultiSentence) sentence).getSentences().get(0));
        }
    }

    private List<Set<Sentence>> divideIntoIndependGroup(Set<Sentence> clauses) {
        List<Set<Sentence>> independGroups = new ArrayList<Set<Sentence>>();

        //TODO: dive into independ groups

        independGroups.add(clauses);
        return independGroups;
    }
}
